
il vous faut installer les dépendances :
```
pip install -r requirements.txt
```
 
Pour lancer le code client :  
````
python main.py
``` 
Pour lancer le code serveur :
```
uvicorn main:app --reload
```

